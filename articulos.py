import sqlite3

class Articulos:

    def abrir(self):
        conexion = sqlite3.connect("escolar")
        return conexion

    def alta(self, datos):
        cone = self.abrir()
        cursor = cone.cursor()
        sql = "INSERT INTO escolar (Nombre, ID, Carrera, Ciclo) VALUES (?, ?, ?, ?)"
        cursor.execute(sql, datos)
        cone.commit()
        cone.close()

    def consulta(self, datos):
        try:
            cone = self.abrir()
            cursor = cone.cursor()
            sql = "SELECT Nombre, Carrera, Ciclo FROM escolar WHERE ID=?"
            cursor.execute(sql, datos)
            return cursor.fetchall()
        finally:
            cone.close()

    def recuperar_todos(self):
        try:
            cone = self.abrir()
            cursor = cone.cursor()
            sql = "SELECT ID, Nombre, Ciclo, Carrera FROM escolar"
            cursor.execute(sql)
            return cursor.fetchall()
        finally:
            cone.close()

    def baja(self, datos):
        try:
            cone = self.abrir()
            cursor = cone.cursor()
            sql = "DELETE FROM escolar WHERE ID=?"
            cursor.execute(sql, datos)
            cone.commit()
            return cursor.rowcount
        except:
            cone.close()

    def modificacion(self, datos):
        try:
            cone = self.abrir()
            cursor = cone.cursor()
            sql = "UPDATE escolar SET Nombre=?, Carrera=? WHERE ID=?"
            cursor.execute(sql, datos)
            cone.commit()
            return cursor.rowcount
        except:
            cone.close()
