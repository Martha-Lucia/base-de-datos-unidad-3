import tkinter as tk
from tkinter import ttk
from tkinter import messagebox as mb
from tkinter import scrolledtext as st
import articulos


class BD:

    def __init__(self):
        self.articulo1 = articulos.Articulos()
        self.ventana1 = tk.Tk()
        self.ventana1.title("SISTEMA ACADEMICO (SA)")
        self.ventana1.configure(background="dark turquoise")
        self.cuaderno1 = ttk.Notebook(self.ventana1)
        self.matricula()
        self.consulta_por_id()
        self.listado_completo()
        self.borrado()
        self.modificar()
        self.cuaderno1.grid(column=0, row=0, padx=10, pady=10)
        self.ventana1.mainloop()

    def matricula(self):
        self.pagina1 = ttk.Frame(self.cuaderno1)
        self.cuaderno1.add(self.pagina1, text="Matricular estudiante")
        self.labelframe1 = ttk.LabelFrame(self.pagina1, text="Usuario")
        self.labelframe1.grid(column=0, row=0, padx=5, pady=10)
        self.label1 = ttk.Label(self.labelframe1, text="Nombre:")
        self.label1.grid(column=0, row=0, padx=4, pady=4)
        self.Nombre = tk.StringVar()
        self.entryNombre = ttk.Entry(self.labelframe1, textvariable=self.Nombre)
        self.entryNombre.grid(column=1, row=0, padx=4, pady=4)
        self.label1 = ttk.Label(self.labelframe1, text="ID:")
        self.label1.grid(column=0, row=1, padx=4, pady=4)
        self.ID = tk.StringVar()
        self.entryID = ttk.Entry(self.labelframe1, textvariable=self.ID)
        self.entryID.grid(column=1, row=1, padx=4, pady=4)
        self.label3 = ttk.Label(self.labelframe1, text="Carrera:")
        self.label3.grid(column=0, row=2, padx=4, pady=4)
        self.carrera = tk.StringVar()
        self.entrycarrera = ttk.Entry(self.labelframe1, textvariable=self.carrera)
        self.entrycarrera.grid(column=1, row=2, padx=4, pady=4)
        self.label4 = ttk.Label(self.labelframe1, text="Ciclo:")
        self.label4.grid(column=0, row=3, padx=4, pady=4)
        self.ciclo = tk.StringVar()
        self.entryciclo = ttk.Entry(self.labelframe1, textvariable=self.ciclo)
        self.entryciclo.grid(column=1, row=3, padx=4, pady=4)
        self.boton1 = ttk.Button(self.labelframe1, text="!! MATRICULAR !!", command=self.agregar)
        self.boton1.grid(column=1, row=4, padx=4, pady=4)

    def agregar(self):
        datos = (self.Nombre.get(), self.ID.get(), self.carrera.get(), self.ciclo.get())
        self.articulo1.alta(datos)
        mb.showinfo("Información", "El estudiante se ha matriculado con éxito")
        self.Nombre.set("")
        self.ID.set("")
        self.ciclo.set("")
        self.carrera.set("")

    def consulta_por_id(self):
        self.pagina2 = ttk.Frame(self.cuaderno1)
        self.cuaderno1.add(self.pagina2, text="Buscar estudiante por ID")
        self.labelframe2 = ttk.LabelFrame(self.pagina2, text="Usuario")
        self.labelframe2.grid(column=0, row=0, padx=5, pady=10)
        self.label1 = ttk.Label(self.labelframe2, text="ID:")
        self.label1.grid(column=0, row=0, padx=4, pady=4)
        self.ID_ = tk.StringVar()
        self.entryID = ttk.Entry(self.labelframe2, textvariable=self.ID_)
        self.entryID.grid(column=1, row=0, padx=4, pady=4)
        self.label2 = ttk.Label(self.labelframe2, text="Nombre:")
        self.label2.grid(column=0, row=1, padx=4, pady=4)
        self.Nombre_ = tk.StringVar()
        self.entryNombre = ttk.Entry(self.labelframe2, textvariable=self.Nombre_, state="readonly")
        self.entryNombre.grid(column=1, row=1, padx=4, pady=4)
        self.label3 = ttk.Label(self.labelframe2, text="Carrera:")
        self.label3.grid(column=0, row=2, padx=4, pady=4)
        self.carrera_ = tk.StringVar()
        self.entrycarrera = ttk.Entry(self.labelframe2, textvariable=self.carrera_, state="readonly")
        self.entrycarrera.grid(column=1, row=2, padx=4, pady=4)
        self.label3 = ttk.Label(self.labelframe2, text="Ciclo:")
        self.label3.grid(column=0, row=3, padx=4, pady=4)
        self.ciclo_ = tk.StringVar()
        self.entryciclo = ttk.Entry(self.labelframe2, textvariable=self.ciclo_, state="readonly")
        self.entryciclo.grid(column=1, row=3, padx=4, pady=4)
        self.boton1 = ttk.Button(self.labelframe2, text="!! CONSULTAR !!", command=self.consultar)
        self.boton1.grid(column=1, row=4, padx=4, pady=4)

    def consultar(self):
        datos = (self.ID_.get(),)
        respuesta = self.articulo1.consulta(datos)
        if len(respuesta) > 0:
            self.Nombre_.set(respuesta[0][0])
            self.carrera_.set(respuesta[0][1])
        else:
            self.Nombre_.set('')
            self.carrera_.set('')
            self.ciclo_.set('')
            mb.showinfo("Información", "No existe un estudiante con el ID que ha ingresado")

    def listado_completo(self):
        self.pagina3 = ttk.Frame(self.cuaderno1)
        self.cuaderno1.add(self.pagina3, text="Lista de Matriculados")
        self.labelframe3 = ttk.LabelFrame(self.pagina3, text="Estudiante")
        self.labelframe3.grid(column=0, row=0, padx=5, pady=10)
        self.boton1 = ttk.Button(self.labelframe3, text="Estudiantes Matriculados", command=self.listar)
        self.boton1.grid(column=0, row=0, padx=4, pady=4)
        self.scrolledtext1 = st.ScrolledText(self.labelframe3, width=30, height=10)
        self.scrolledtext1.grid(column=0, row=1, padx=10, pady=10)

    def listar(self):
        respuesta = self.articulo1.recuperar_todos()
        self.scrolledtext1.delete("1.0", tk.END)
        for fila in respuesta:
            self.scrolledtext1.insert(tk.END, "ID : " + str(fila[0]) +
                                      "\nNombre : " + fila[1] +
                                      "\nCiclo : " + str(fila[2]) +
                                      "\nCarrera : " + str(fila[3]) +
                                      "\n\n")

    def borrado(self):
        self.pagina4 = ttk.Frame(self.cuaderno1)
        self.cuaderno1.add(self.pagina4, text="Eliminar estudiante por ID")
        self.labelframe4 = ttk.LabelFrame(self.pagina4, text="Estudiante:")
        self.labelframe4.grid(column=0, row=0, padx=5, pady=10)
        self.label1 = ttk.Label(self.labelframe4, text="ID:")
        self.label1.grid(column=0, row=0, padx=4, pady=4)
        self.codigoborra = tk.StringVar()
        self.entryborra = ttk.Entry(self.labelframe4, textvariable=self.codigoborra)
        self.entryborra.grid(column=1, row=0, padx=4, pady=4)
        self.boton1 = ttk.Button(self.labelframe4, text="!! ELIMINAR !!", command=self.borrar)
        self.boton1.grid(column=1, row=1, padx=4, pady=4)

    def borrar(self):
        datos = (self.codigoborra.get(),)
        cantidad = self.articulo1.baja(datos)
        if cantidad == 1:
            mb.showinfo("Información", "Se eliminó el estudiante matriculado")
        else:
            mb.showinfo("Información", "No se encuentra, no se puede eliminar")

    def modificar(self):
        self.pagina5 = ttk.Frame(self.cuaderno1)
        self.cuaderno1.add(self.pagina5, text="Modificar datos del estudiante")
        self.labelframe5 = ttk.LabelFrame(self.pagina5, text="Datos")
        self.labelframe5.grid(column=0, row=0, padx=5, pady=10)
        self.label1 = ttk.Label(self.labelframe5, text="ID:")
        self.label1.grid(column=0, row=0, padx=4, pady=4)
        self.IDmod = tk.StringVar()
        self.entryID = ttk.Entry(self.labelframe5, textvariable=self.IDmod)
        self.entryID.grid(column=1, row=0, padx=4, pady=4)
        self.label2 = ttk.Label(self.labelframe5, text="Nombre:")
        self.label2.grid(column=0, row=1, padx=4, pady=4)
        self.Nombremod = tk.StringVar()
        self.entryNombre = ttk.Entry(self.labelframe5, textvariable=self.Nombremod)
        self.entryNombre.grid(column=1, row=1, padx=4, pady=4)
        self.label3 = ttk.Label(self.labelframe5, text="Carrera:")
        self.label3.grid(column=0, row=2, padx=4, pady=4)
        self.carreramod = tk.StringVar()
        self.entrycarrera = ttk.Entry(self.labelframe5, textvariable=self.carreramod)
        self.entrycarrera.grid(column=1, row=2, padx=4, pady=4)
        self.label4 = ttk.Label(self.labelframe5, text="Ciclo:")
        self.label4.grid(column=0, row=3, padx=4, pady=4)
        self.ciclomod = tk.StringVar()
        self.entryciclo = ttk.Entry(self.labelframe5, textvariable=self.ciclomod)
        self.entryciclo.grid(column=1, row=3, padx=4, pady=4)
        self.boton1 = ttk.Button(self.labelframe5, text="!! CONSULTAR !!", command=self.consultar_mod)
        self.boton1.grid(column=1, row=4, padx=4, pady=4)
        self.boton1 = ttk.Button(self.labelframe5, text="!! MODIFICAR !!", command=self.modifica)
        self.boton1.grid(column=1, row=5, padx=4, pady=4)

    def modifica(self):
        datos = (self.IDmod.get(), self.Nombremod.get(), self.carreramod.get(), self.ciclomod.get())
        cantidad = self.articulo1.modificacion(datos)
        if cantidad == 1:
            mb.showinfo("Información", "Se modificó la información del estudiante")
        else:
            mb.showinfo("Información", "No existe un estudiante con el ID que ha ingresado")

    def consultar_mod(self):
        datos = (self.IDmod.get(),)
        respuesta = self.articulo1.consulta(datos)
        if len(respuesta) > 0:
            self.Nombremod.set(respuesta[0][0])
            self.carreramod.set(respuesta[0][1])
            self.ciclomod.set(respuesta[0][2])
        else:
            self.Nombremod.set('')
            self.carreramod.set('')
            self.ciclomod.set('')
            mb.showinfo("Información", "No existe un estudiante con el ID ingresado")


aplicacion1 = BD()